package Task3.Products;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class ConsignmentTests {

    private  PackagedProduct[] products = new PackagedProduct[3];
    private Consignment consignment;

    private final String description = "Description";
    private final String title = "Title";
    private final int weight =10;
    private final int quantity = 5;
    @Test
    public void ConstructorFail(){
        assertAll(
                ()->assertThrows(ProductException.class,()->new Consignment(null,null)),
                ()->assertThrows(ProductException.class,()->new Consignment("",products))
        );
    }

    @Test
    public void ConstructorTest() throws ProductException {

        products[0]= new PackagedWeightProduct(new WeightProduct(title,description),
                weight,new ProductPackaging(description,weight));
        products[1]= new PackagedPieceProduct(new PieceProduct(title,description,weight),weight,
                new ProductPackaging(description,weight));
        products[2] = new PackagedProduct(title,description,new ProductPackaging(description,weight));

        consignment =new Consignment(description,products);
        assertEquals(30,consignment.getWeight());

    }
}
