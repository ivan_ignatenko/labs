package Task3.Products;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class PackagedPieceProductTests {
    private ProductPackaging productPackaging;
    private PieceProduct pieceProduct;
    private PackagedPieceProduct packagedPieceProduct;

    private final double weight =200;
    private final int quantity = 5;
    private final String expectedTitle ="Title";
    private final String expectedDescription = "Description";

    @Test
    public void ConstructorFailTest(){
        assertAll(
                ()->assertThrows(ProductException.class,()->new PackagedPieceProduct(null,-5,null)),
                ()->assertThrows(ProductException.class,()->new PackagedPieceProduct(null,4,null))
        );
    }
    @Test
    public void ConstructorTest() throws ProductException {
        productPackaging = new ProductPackaging(expectedTitle,weight);
        pieceProduct = new PieceProduct(expectedTitle,expectedDescription,weight);
        packagedPieceProduct = new PackagedPieceProduct(pieceProduct,quantity,productPackaging);
        assertAll(
                ()->assertEquals(quantity,packagedPieceProduct.getQuantity()),
                ()->assertEquals(quantity*weight,packagedPieceProduct.getNetto()),
                ()->assertEquals(quantity*weight+weight,packagedPieceProduct.getWeight())
        );
    }
}
