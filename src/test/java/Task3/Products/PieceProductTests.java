package Task3.Products;
import org.junit.BeforeClass;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;

public class PieceProductTests {
    @Mock
    private PieceProduct pieceProduct = mock(PieceProduct.class);
    private String expectedDescription = "Tasty";
    private String expectedTitile = "Broad";

    @BeforeClass
    public void setUp(){
        MockitoAnnotations.initMocks(this);
    }
    @Test
    public void ConstructorFailTest(){
        assertAll(
                ()-> assertThrows(ProductException.class,()->new PieceProduct(expectedTitile,"",5)),
                ()->assertThrows(ProductException.class,()->new PieceProduct("",expectedDescription,20)),
                ()->assertThrows(ProductException.class,()->new PieceProduct(null,expectedDescription,20)),
                ()->assertThrows(ProductException.class,()->new PieceProduct(expectedTitile,null,20)),
                ()->assertThrows(ProductException.class,()->new PieceProduct(expectedTitile,expectedDescription,-2))
        );
    }
    @Test
    public void getWeightTest(){
        Mockito.when(pieceProduct.getWeight()).thenReturn((double) 2).thenReturn((double) 5).thenReturn(0.001);
        assertAll(
                ()->assertNotNull(pieceProduct),
                ()->assertEquals(2.,pieceProduct.getWeight()),
                ()->assertEquals(5,pieceProduct.getWeight()),
                ()->assertEquals(0.001,pieceProduct.getWeight())
        );
    }

    @Test
    public void ConstructorTest() throws ProductException {
        PieceProduct pieceProduct2 = new PieceProduct(expectedTitile,expectedDescription,200);
        assertAll(
                ()->assertNotNull(pieceProduct2),
                ()->assertEquals(pieceProduct2.getClass(),PieceProduct.class),
                ()->assertEquals(expectedTitile,pieceProduct2.getTitle()),
                ()->assertEquals(expectedDescription,pieceProduct2.getDescription()),
                ()->assertEquals(200,pieceProduct2.getWeight())
        );
    }
}
