package Task3.Products;

import Task3.Products.Filter.*;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class FilterTests {
    @Test
    public void beginStringTest() throws FilterException {
        BeginStringFilter beginStringFilter = new BeginStringFilter("abc");
        assertTrue(beginStringFilter.apply("abcdefg"));
        assertFalse(beginStringFilter.apply("ttt"));
        assertFalse(beginStringFilter.apply(""));
    }
    @Test
    public void endStringTest() throws FilterException {
        EndStringFilter endStringFilter = new EndStringFilter("abc");
        assertTrue(endStringFilter.apply("edfg1231abc"));
        assertFalse(endStringFilter.apply("adsadsads"));
    }
    @Test
    public void substringFilterTest() throws FilterException {
        SubstringFilter substringFilter = new SubstringFilter("abc");
        assertAll(
                ()->assertTrue(substringFilter.apply("adsadaaasdasddaabcadsasd")),
                ()->assertTrue(substringFilter.apply("abcabcabc1213312")),
                ()->assertTrue(substringFilter.apply("abc1231230")),
                ()->assertTrue(substringFilter.apply("12331231asdasdabc")),
                ()->assertFalse(substringFilter.apply("ttttttttt")),
                ()->assertFalse(substringFilter.apply(""))
        );
    }
    @Test
    public void goodsService() throws ProductException, FilterException {
        String description ="1";
        int weight = 200;
        PackagedProduct[] products = new PackagedProduct[3];
        products[0]= new PackagedWeightProduct(new WeightProduct("abc",description),
                weight,new ProductPackaging(description,weight));
        products[1]= new PackagedPieceProduct(new PieceProduct("tttdef",description,weight),weight,
                new ProductPackaging(description,weight));
        products[2] = new PackagedProduct("abcdef",description,new ProductPackaging(description,weight));

       Consignment consignment =new Consignment(description,products);
        assertAll(
               ()->assertEquals(2, GoodsService.countByFilter(consignment,new BeginStringFilter("abc"))),
               ()->assertEquals(2,GoodsService.countByFilter(consignment,new EndStringFilter("def"))),
               ()->assertFalse(GoodsService.checkAllWeighted(consignment))
       );
    }
}
