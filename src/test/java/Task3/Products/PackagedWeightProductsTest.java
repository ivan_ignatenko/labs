package Task3.Products;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class PackagedWeightProductsTest {
    private WeightProduct weightProduct;
    private ProductPackaging productPackaging;
    private PackagedWeightProduct packagedWeightProduct;

    private final double weight = 200;
    private final String expectedTitle ="Title";
    private final String expectedDescription = "Description";

    @Test
    public void ConstructorFailTest() throws ProductException {
        assertAll(
                ()->assertThrows(ProductException.class,()->new PackagedWeightProduct(null,-200,null)),
                ()->assertThrows(ProductException.class,()->new PackagedWeightProduct(null)),
                ()->assertThrows(ProductException.class,()->new PackagedWeightProduct(weightProduct,weight,productPackaging))
        );
        weightProduct = new WeightProduct(expectedTitle,expectedDescription);
        assertAll(
                ()->assertThrows(ProductException.class,()->new PackagedWeightProduct(weightProduct,-100,productPackaging)),
                ()->assertThrows(ProductException.class,()->new PackagedWeightProduct(weightProduct,100,productPackaging))
        );
    }
    @Test
    public void ConstructorTest() throws ProductException {
        weightProduct  =new WeightProduct(expectedTitle,expectedDescription);
        productPackaging = new ProductPackaging(expectedTitle,weight);
        packagedWeightProduct= new PackagedWeightProduct(weightProduct,weight,productPackaging);
        assertAll(
                ()->assertNotNull(packagedWeightProduct),
                ()->assertEquals(expectedTitle,packagedWeightProduct.getTitle()),
                ()->assertEquals(expectedDescription,packagedWeightProduct.getDescription()),
                ()->assertEquals(weight,packagedWeightProduct.getNetto()),
                ()->assertEquals(2*weight,packagedWeightProduct.getWeight())
        );
    }
}
