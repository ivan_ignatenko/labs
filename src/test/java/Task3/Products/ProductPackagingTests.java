package Task3.Products;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class ProductPackagingTests {

    private final String expectedTitle = "Title";
    private final int expectedWeight = 20;
    private ProductPackaging productPackaging;
    @Test
    public void ConstructorFailTest(){
        assertAll(
                ()->assertThrows(ProductException.class,()->new ProductPackaging(expectedTitle,-1)),
                ()->assertThrows(ProductException.class,()->new ProductPackaging("",12)),
                ()->assertThrows(ProductException.class,()->new ProductPackaging(null,34)),
                ()->assertThrows(ProductException.class,()->new ProductPackaging(null,-2)),
                ()->assertThrows(ProductException.class,()->new ProductPackaging("",20))
        );
    }
    @Test
    public void ConstructorTest() throws ProductException {
        productPackaging = new ProductPackaging(expectedTitle,expectedWeight);
        assertAll(
                ()->assertNotNull(productPackaging),
                () -> assertEquals(productPackaging.getClass(), ProductPackaging.class),
                ()->assertEquals(productPackaging.getTitle(),expectedTitle),
                ()->assertEquals(productPackaging.getWeight(),expectedWeight)
        );
    }

}
