package Task3.Products;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class PackagedProductTests {
    private final String expectedTitle = "Title";
    private final String expectedDescription = "Description";
    private final int expectedWeight =200;

    private PackagedProduct packagedProduct;
    private ProductPackaging productPackaging;
    private Product product;

    @Test
    public void ConstructorFailTest() throws ProductException {
        assertAll(
                ()->assertThrows(ProductException.class,()->new PackagedProduct(null,null,null)),
                ()->assertThrows(ProductException.class,()->new PackagedProduct(null)),
                ()->assertThrows(ProductException.class,()->new PackagedProduct(expectedTitle,expectedDescription,productPackaging))
        );
        productPackaging = new ProductPackaging(expectedTitle,expectedWeight);
        assertAll(
                ()->assertThrows(ProductException.class,()->new PackagedProduct("","",productPackaging)),
                ()->assertThrows(ProductException.class,()->new PackagedProduct(null,null,productPackaging)),
                ()->assertThrows(ProductException.class,()->new PackagedProduct(expectedTitle,"",productPackaging)),
                ()->assertThrows(ProductException.class,()->new PackagedProduct("",expectedDescription,productPackaging))
        );
    }
    @Test
    public void ConstructorTest() throws ProductException {
        productPackaging = new ProductPackaging(expectedTitle,expectedWeight);
        packagedProduct = new PackagedProduct(expectedTitle,expectedDescription,productPackaging);
        assertAll(
                ()->assertNotNull(packagedProduct),
                ()->assertEquals(packagedProduct.getClass(),PackagedProduct.class),
                ()->assertTrue(packagedProduct instanceof IWeightProduct),
                ()->assertTrue(packagedProduct instanceof Product),
                ()->assertEquals(expectedWeight,packagedProduct.getWeight()),
                ()->assertEquals(productPackaging,packagedProduct.getPackaging())
        );
    }

    @Test
    public void ConstructorTest2() throws ProductException {
        productPackaging = new ProductPackaging(expectedTitle,expectedWeight);
        product = new Product(expectedTitle,expectedDescription);
        packagedProduct = new PackagedProduct(product,productPackaging);
        assertAll(
                ()->assertNotNull(packagedProduct),
                ()->assertEquals(packagedProduct.getClass(),PackagedProduct.class),
                ()->assertEquals(expectedWeight,packagedProduct.getWeight()),
                ()->assertEquals(productPackaging,packagedProduct.getPackaging())
        );
    }
}
