package Task3.Products;

import org.junit.BeforeClass;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;


public class ProductTests{
    @Mock
    private String expectedDescription = "Tasty";
    private String expectedTitile = "Broad";
    private Product product = mock(Product.class);

    @BeforeClass
    public void setUp(){
        MockitoAnnotations.initMocks(this);
    }
    @Test
    public void ProductFailConstructorTest(){
        assertAll(
                ()->assertThrows(ProductException.class,()->new Product(null,"")),
                ()->assertThrows(ProductException.class,()->new Product(null)),
                ()->assertThrows(ProductException.class,()->new Product("Burzum-ishi",""))
        );
    }
    @Test
    public void ProductConstructorTest() throws ProductException {
        Mockito.when(product.getTitle()).thenReturn(expectedTitile);
        Mockito.when(product.getDescription()).thenReturn(expectedDescription);
        assertAll(
                ()->assertEquals(expectedDescription,product.getDescription()),
                ()->assertEquals(expectedTitile,product.getTitle())
        );
    }
}
