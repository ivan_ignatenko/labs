package Task5.Matrix;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class MatrixTest {
    @Test
    public void matrixTest() throws MatrixException {
        final Matrix mat = new Matrix(3);
        mat.set(0,0,1); mat.set(0,1, 2); mat.set(0,2, 3);
        mat.set(1,0,4); mat.set(1,1,5); mat.set(1,2, 6);
        mat.set(2,0,7); mat.set(2,1,8); mat.set(2,2,9);
        final Matrix mat1 = new Matrix(3);
        mat1.set(0,0,0); mat1.set(0,1, 0); mat1.set(0,2, 0);
        mat1.set(1,0,4); mat1.set(1,1,5); mat1.set(1,2, 6);
        mat1.set(2,0,7); mat1.set(2,1,8); mat1.set(2,2,9);
        final Matrix mat2 = new Matrix(3);
        mat2.set(0,0,1); mat2.set(0,1, 0); mat2.set(0,2, 0);
        mat2.set(1,0,0); mat2.set(1,1,1); mat2.set(1,2, 0);
        mat2.set(2,0,0); mat2.set(2,1,0); mat2.set(2,2,1);
        assertAll(
                ()->assertEquals(0,mat.getDet()),
                ()->assertEquals(0,mat1.getDet()),
                ()->assertEquals(1,mat2.getDet()),
                ()->assertEquals(1, mat2.get(0,0)),
                ()->assertEquals(8, mat.get(2,1))
        );
        assertThrows(MatrixException.class, Matrix::new);
        assertThrows(MatrixException.class, ()->new Matrix(0));
        assertThrows(MatrixException.class, ()->new Matrix(-5));
        assertThrows(MatrixException.class, ()->new Matrix((Matrix) null));
        assertThrows(MatrixException.class, ()->mat.set(5,5,4));
        assertThrows(MatrixException.class, ()->mat.get(5,5));
    }

    @Test
    public void diagMatrixTest() throws MatrixException {
        final DiagMatrix mat = new DiagMatrix(1,2,3,4);
        assertAll(
                ()->assertEquals(24, mat.getDet()),
                ()->assertEquals(4, mat.get(3,3))
        );
        assertThrows(MatrixException.class, ()->new UpTriangleMatrix(0));
        assertThrows(MatrixException.class, ()->mat.set(1,3,5));
        assertThrows(MatrixException.class, ()->mat.set(2,6,8));
    }

    @Test
    public void upTriangleMatrixTest() throws MatrixException{
        final UpTriangleMatrix mat = new UpTriangleMatrix(3);
        mat.set(0,0,1); mat.set(0,1, 2); mat.set(0,2, 3);
        mat.set(1,1,7); mat.set(1,2, 5); mat.set(2,2,11);
        assertAll(
                ()->assertEquals(77, mat.getDet()),
                ()->assertEquals(11, mat.get(2,2))
        );
        assertThrows(MatrixException.class, ()->mat.set(2,1, 7));
    }

    @Test
    public void matrixServiceTest() throws MatrixException {
        final Matrix mat1 = new Matrix(3); //0
        mat1.set(0,0,1); mat1.set(0,1, 2); mat1.set(0,2, 3);
        mat1.set(1,0,4); mat1.set(1,1,5); mat1.set(1,2, 6);
        mat1.set(2,0,7); mat1.set(2,1,8); mat1.set(2,2,9);
        final Matrix mat2 = new Matrix(3);//225
        mat2.set(0,0,5); mat2.set(0,1, 2); mat2.set(0,2, 3);
        mat2.set(1,0,0); mat2.set(1,1,5); mat2.set(1,2, 6);
        mat2.set(2,0,0); mat2.set(2,1,0); mat2.set(2,2,9);
        final UpTriangleMatrix mat3 = new UpTriangleMatrix(3); //77
        mat3.set(0,0,1); mat3.set(0,1, 2); mat3.set(0,2, 3);
        mat3.set(1,1,7); mat3.set(1,2, 5); mat3.set(2,2,11);
        final DiagMatrix mat4 = new DiagMatrix(7,5,9,10);
        Matrix[] mats = new Matrix[] {mat1, mat3, mat2, mat4};
        Matrix[] mats1 = new Matrix[] {mat2, mat1, mat3, mat4};
        MatrixService.arrangeMatrices(mats1);
        assertArrayEquals(mats, mats1);
    }
}
