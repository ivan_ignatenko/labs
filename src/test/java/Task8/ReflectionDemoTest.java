package Task8;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.api.Test;

import java.sql.Time;
import java.util.*;

import static org.junit.jupiter.api.Assertions.*;

public class ReflectionDemoTest {

    @Test
    public void testCountOfHuman(){
        class Biker extends Person{
            public Biker(String name, int age) {
                super(name, age);
            }
        }
        class Sample{}

        Biker b1 = new Biker("Ben", 23);
        Person p1 = new Person("Gleb", 11);
        Sample sam1 = new Sample();
        Object ob1 = new Object();
        String str1 = "1";
        int ans = ReflectionDemo.countOfHumans(ob1, str1, sam1, p1, b1);

        assertEquals(2, ans);

    }

    @Test
    public void testMethodsOfObject(){
        class Glass {
            public int getInt() {
                return 1;
            }
            private int getSecondInt() {
                return 12;
            }
            @NotNull
            @Contract(pure = true)
            private String getStr() {
                return "as";
            }
            public void setM(){}
            void aVoid() { }
            public void aSet() {
            }
        }

        Glass s = new Glass();
        List<String> res = new LinkedList<>();
        res.add("getInt");
        res.add("setM");
        res.add("aSet");
        assertEquals(res, ReflectionDemo.methodsOfObject(s));
    }

    @Test
    public void testAllSuperClassesOfObject(){
        ArrayList<Integer> l1 = new ArrayList<>();
        List<String> res = ReflectionDemo.allSuperClassesOfObject(l1);
        List<String> actRes = Arrays.asList("AbstractSequentialList", "AbstractList", "AbstractCollection", "Object");

        assertFalse(res.containsAll(actRes));

        Time t1 = new Time(1);
        List<String> res1 = ReflectionDemo.allSuperClassesOfObject(t1);
        List<String> actRes1 = Arrays.asList("Date", "Object");
        assertTrue(res1.containsAll(actRes1));

    }

    @Test
    public void testCountOfInterface(){
        class Sample implements Executable{
            @Override
            public void execute() { System.out.println(1);}
        }

        class Rider implements Executable{
            @Override
            public void execute() {
                System.out.println(2);
            }
        }

        class Clones implements Executable{
            @Override
            public void execute() {
                System.out.println(3);
            }
        }

        Sample sample = new Sample();
        Clones clones = new Clones();
        Rider rider = new Rider();
        Rider rider1 = new Rider();
        String s1 = "aq";
        int int1 = 5;
        String s2 = "th";
        boolean b1 = true;

        assertEquals(4, ReflectionDemo.countOfInterface(sample, clones, rider, rider1, s1, int1, s2, b1));

    }

    @Test
    public void testGetObjectSettersGetters1(){
        class Sample{
            private int number;
            private String string;
            Sample(int number){
                this.number = number;
                this.string = "";
            }

            public void setNumber(int number) {
                this.number = number;
            }

            public int getNumber() {
                return number;
            }

            public void setString(String string){
                this.string = string;
            }

            public void setValue(int value){}

            int getR(){return 1;}

            public void setRace(int race){}
        }

        Sample sample = new Sample(1);

        List<String> res = new ArrayList<>();
        res.add("setValue");
        res.add("getNumber");

        res.add("setNumber");
        res.add("setString");
        res.add("setRace");

        assertEquals(res, ReflectionDemo.getObjectSettersGetters(sample));

    }

    @Test
    public void testGetObjectSettersGetters2(){

        Demo sample = new Demo(1,2.);

        List<String> res = new ArrayList<>();
        res.add("setRp");
        res.add("setKilo");
        res.add("getRp");
        res.add("getKilo");
        res.add("getS");

        assertEquals(res, ReflectionDemo.getObjectSettersGetters(sample));

    }



    public static void main(String[] args) {

    }


}
