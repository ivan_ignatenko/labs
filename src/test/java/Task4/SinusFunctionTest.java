package Task4;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SinusFunctionTest {

    @Test
    void testConstructor(){
        assertThrows(FunctionException.class, ()->new LinearFunction(12, 4, 10, 1));
        assertThrows(FunctionException.class, ()->new LinearFunction(0, 4, 10, 12));
    }

    @Test
    void getValue() throws FunctionException {

        double epsilon = 10e-7;
        SinusFunction test = new SinusFunction(1, 2, 0, 5);
        assertThrows(FunctionException.class, ()->test.getValue(7));
        assertTrue(Math.abs(test.getValue(Math.PI)) < epsilon);
        assertTrue(Math.abs(test.getValue(Math.PI/4) -1 ) < epsilon);
    }
}