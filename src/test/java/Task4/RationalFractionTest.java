package Task4;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RationalFractionTest {

    @Test
    void testConstructor(){
        assertThrows(FunctionException.class, ()->
                new RationalFraction(new LinearFunction(1, 2, 0, 8),
                        new LinearFunction(2,42, 1, 11)));

    }

    @Test
    void getValue() throws FunctionException {
        double epsilon = 10e-7;
        RationalFraction test = new RationalFraction(new LinearFunction(2, 5, 0, 10),
                new LinearFunction(1,2 ,0, 10));
        assertThrows(FunctionException.class, ()-> test.getValue(-2));
        assertTrue(Math.abs(test.getValue(2)-2.25) < epsilon);
    }
}