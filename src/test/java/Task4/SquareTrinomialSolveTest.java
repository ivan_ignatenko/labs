package Task4;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class SquareTrinomialSolveTest {
    @Test
    void solveTest() throws SquareException, FunctionException {
        SquareTrinomialSolver solver = new SquareTrinomialSolver(new SquareTrinomial(5,6,1));
        double[] nums = new double[]{-0.2,-1};
        assertArrayEquals(nums,solver.solve());
        SquareTrinomialSolver solver1 = new SquareTrinomialSolver(new SquareTrinomial(1,2,3));
        assertThrows(FunctionException.class,solver1::solve);
    }
}
