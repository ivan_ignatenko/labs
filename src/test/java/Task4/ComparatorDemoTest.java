package Task4;
import Task3.Products.PieceProduct;
import Task3.Products.Product;
import Task3.Products.ProductException;
import Task3.Products.WeightProduct;
import org.junit.jupiter.api.Test;

import java.util.Comparator;

import static org.junit.jupiter.api.Assertions.*;

class ComparatorDemoTest {

    @Test
    void sortGoods() throws ProductException {
        Product test = new Product("drs", "bg");
        Product test1 = new WeightProduct("vba", "ca");
        Product test2 = new PieceProduct("abc", "bt",5);
        Product test3 = new Product("ci", "dtz");
        Product test4 = new PieceProduct("vba", "ars", 3);
        Product[] arr = {test, test1, test2, test3, test4};
        Comparator<Product> comp = new ComparatorProduct<>();
        ComparatorDemo.sortGoods(arr, comp);
        assertAll(
                ()->assertEquals("abc", arr[0].getTitle()),
                ()->assertEquals("ci", arr[1].getTitle()),
                ()->assertEquals("drs",arr[2].getTitle()),
                ()->assertEquals("ars",arr[3].getDescription()),
                ()->assertEquals("ca",arr[4].getDescription())
        );
    }
}