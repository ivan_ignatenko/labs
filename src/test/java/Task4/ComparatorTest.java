package Task4;
import Task3.Products.Product;
import Task3.Products.ProductException;
import org.junit.jupiter.api.Test;

import java.util.Comparator;

import static org.junit.jupiter.api.Assertions.*;

class ComparatorTest {

    @Test
    void compare() throws FunctionException, ProductException {
        Product test = new Product("Car", "audi");
        Product test1 = new Product("Domino" , "1-2-3-4-5-6");
        Product test2 = new Product("Car", "cmw");
        Comparator<Product> testComp = new ComparatorProduct<>();
        assertEquals(-1, testComp.compare(test, test1));
        assertEquals(-2 ,testComp.compare(test,test2));
    }
}