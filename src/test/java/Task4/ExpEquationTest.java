package Task4;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExpEquationTest {

    @Test
    void Constructor() throws FunctionException {
        assertThrows(FunctionException.class, ()->new ExpEquation(12, 4, 10, 1));
        assertThrows(FunctionException.class, ()->new ExpEquation(0, 4, 10, 12));
    }

    @Test
    void getValue() throws FunctionException {
        double epsilon = 10e-8;
        ExpEquation test = new ExpEquation(1, 1, -100, 100);
        assertThrows(FunctionException.class, () -> test.getValue(-101));
        assertTrue(Math.abs(test.getValue(1) -3.71828182846) < epsilon);
    }
}