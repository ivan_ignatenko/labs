package Task1;
;
import org.junit.Assert;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


public class TestClass {
    @Test
    public void testVectorSum(){
        Vector3D v1 = new Vector3D(1,0,0);
        Vector3D v2 = new Vector3D(0,1,1);
        Assert.assertEquals(new Vector3D(1,1,1), Vector3DProcessor.sumVector(v1,v2));
    }
    @Test
    public void testVectorSub1(){
        Vector3D v1= new Vector3D(1,3,5);
        Vector3D v2 = new Vector3D(3,4,5);
        Assert.assertEquals(new Vector3D(2,1,0),Vector3DProcessor.subVector(v2,v1));
    }
    @Test
    public void testVectorSub2(){
        Vector3D v1= new Vector3D(1,3,5);
        Vector3D v2= new Vector3D(3,4,5);
        Assert.assertEquals(new Vector3D(-2,-1,0),Vector3DProcessor.subVector(v1,v2));
    }
    @Test
   public void testVectorScalarMul1(){
        Vector3D v1 = new Vector3D(1,1,1);
        Vector3D v2 = new Vector3D(1,2,3);
        Assert.assertEquals(6.,Vector3DProcessor.scalarMul(v1,v2),1e-12);
    }
    @Test
    public void testVectorScalarMul2(){
        Vector3D v1 = new Vector3D(1,2,3);
        Vector3D v2 = new Vector3D(3,6,8);
        Assert.assertEquals(39.,Vector3DProcessor.scalarMul(v1,v2),1e-12);
    }
    @Test
    public void Collinearity(){
        Vector3D v1 = new Vector3D(1,2,3);
        Vector3D v2 = new Vector3D(2,4,6);
        Vector3D v3 = new Vector3D(3,-1,5);
        assertAll(
                ()->assertTrue(Vector3DProcessor.collinearity(v1,v2)),
                ()->assertFalse(Vector3DProcessor.collinearity(v2,v3)),
                ()->assertFalse(Vector3DProcessor.collinearity(v1,v3))
        );
    }

}
