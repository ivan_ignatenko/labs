package Task2;

import Task2.Exception.FinanceExeption;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class FinanceTests {
    private final String creator_1 = "Adam Smith";
    private final String creator_2 = "Snaga Boromir";

    @Test
    void testPaymentConstructor() throws FinanceExeption {
        Payment payment1 = new Payment(creator_1, "10.1.20", 100);
        assertAll(
                ()->assertNotNull(payment1),
                ()-> assertEquals(10,payment1.getData().getYear()),
                ()-> assertEquals(1,payment1.getData().getMouth()),
                ()->assertEquals(20,payment1.getData().getDay()),
                ()->assertEquals(creator_1,payment1.getName())
        );
        assertThrows(FinanceExeption.class,()-> new Payment(creator_2,new Date(1000,10000,1000),1000));
        Payment payment2 = new Payment(creator_2,new Date(10,1,20),1000);
        assertAll(
                ()->assertNotNull(payment2),
                ()-> assertEquals(10,payment2.getData().getYear()),
                ()-> assertEquals(1,payment2.getData().getMouth()),
                ()->assertEquals(20,payment2.getData().getDay()),
                ()->assertEquals(creator_2,payment2.getName())
        );
    }
    @Test
    public void FinanceProcessorTestAllPaymentsForThisLetter() throws FinanceExeption {
        FinanceReport financeReport = new FinanceReport(new Payment[]{
                new Payment("abc",1,2,3,400),
                new Payment("adf",1,2,3,300),
                new Payment("bcde",2,3,4,200)
        },"creator",new Date(10,10,10));
        FinanceReport expected1 = new FinanceReport(new Payment[]{
                new Payment("abc",1,2,3,400),
                new Payment("adf",1,2,3,300)},"creator",new Date(10,10,10));
        Assertions.assertEquals(expected1, FinanceReportProcessor.allPaymentsForThisLetter(financeReport,'a'));
        FinanceReport expected2 = new FinanceReport(new Payment[]{
                new Payment("adf",1,2,3,300),
                new Payment("bcde",2,3,4,200)
        },"creator",new Date(10,10,10));

        assertEquals(expected2,FinanceReportProcessor.allPaymentLessThisPayment(financeReport,300));
        assertEquals(700,FinanceReportProcessor.totalPaymentOnThisDate(financeReport,"3.2.1"));
       // assertEquals("January April May June July August September October December ",
            //    FinanceReportProcessor.monthInWhichThereWasNoPayment(financeReport,1));
    }

}