package Task2;


import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class StringProcessorTestJunit {
    @Test
    public void copy()throws IllegalArgumentException{
        assertAll(
                ()-> Assertions.assertEquals("1111", StringProcessor.stringCopy("1",4)),
                ()->assertEquals("2323",StringProcessor.stringCopy("23",2)),
                ()->assertEquals("",StringProcessor.stringCopy("123",0))
        );
    }
    @Test
    public void numberOfEntries(){
        assertAll(
                ()->assertEquals(1,StringProcessor.numberOfEntries("abcdef","ef")),
                ()->assertEquals(2,StringProcessor.numberOfEntries("aabcaabcd","aabc")),
                ()->assertEquals(0,StringProcessor.numberOfEntries("123","a")))
        ;
    }
    @Test
    public void replaceStringTest(){
        String s ="abc def g";
        String s2="123 456 789 10";
        assertEquals("g def abc",StringProcessor.replaceString(new StringBuilder(s)).toString());
        assertEquals("10 456 789 123",StringProcessor.replaceString(new StringBuilder(s2)).toString());
    }
    @Test
    public void ConvertNumbersTest(){
        String s = "Васе 0x00000010 лет";
        assertEquals("Васе 16 лет ",StringProcessor.convertSexteenNumberToUsualNumber(s));

        String s1 = "Васе 0x00000011 лет";
        assertEquals("Васе 17 лет ",StringProcessor.convertSexteenNumberToUsualNumber(s1));

        String s2 = "Васе 0x00000011 лет , Андрею 0x0000010 лет";
        assertEquals("Васе 17 лет , Андрею 16 лет ",StringProcessor.convertSexteenNumberToUsualNumber(s2));
    }
}
