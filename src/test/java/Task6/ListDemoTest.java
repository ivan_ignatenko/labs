package Task6;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;

class ListDemoTest {

    final Human h181 = new Human("aaa","baa","q",18);
    final Human h201 = new Human("rrr","baa","q",20);
    final Human h202 = new Human("ree","taa","q",20);
    final Human h211 = new Human("ree","aaa","q",21);
    final Human h182 = new Human("a","b","q",18);

    ListDemoTest() throws HumanException {
    }

    @BeforeEach
    void setUp() {

    }


    @Test
    void namesakeTest() throws HumanException {
        List<Human> actual = new LinkedList<>();
        actual.add(h181);
        actual.add(h201);
        actual.add(h211);
        actual.add(h202);
        List<Human> expected = new LinkedList<>();
        expected.add(new Human("aaa","baa","q",18));
        expected.add(new Human("rrr","baa","q",20));
        assertEquals(expected,ListDemo.namesake(actual,new Human("t","baa","y",50)));
    }

    @Test
    void withoutThisPerson1() {
        List<Human> actual = new LinkedList<>();
        actual.add(h181);
        actual.add(h201);
        actual.add(h211);
        actual.add(h202);
        List<Human> expected = new LinkedList<>();
        expected.add(h201);
        expected.add(h211);
        expected.add(h202);
        assertEquals(expected,ListDemo.withoutThisPerson(actual,h181));
    }

    @Test
    void withoutThisPerson2() {
        List<Human> actual = new LinkedList<>();
        actual.add(h181);
        assertEquals(
                new LinkedList<>(),ListDemo.withoutThisPerson(actual,h181));
    }

    @Test
    void addition() {
        Set<Integer> set1 = new HashSet<>();
        set1.add(1);
        set1.add(2);
        set1.add(3);
        Set<Integer> set2 = new HashSet<>();
        set2.add(1);
        set2.add(4);
        set2.add(5);
        Set<Integer> set3 = new HashSet<>();
        set3.add(4);
        set3.add(6);
        set3.add(7);
        Set<Integer> set4 = new HashSet<>();
        set4.add(8); //
        set4.add(9);//
        Set<Integer> setTest = new HashSet<>();

        setTest.add(1);
        setTest.add(2);
        setTest.add(3);
        setTest.add(4);
        setTest.add(5);
        setTest.add(6);

        List<Set<Integer>> expected = new LinkedList<>();
        expected.add(set4);
        List<Set<Integer>> list = new LinkedList<>();

        list.add(set1);
        list.add(set2);
        list.add(set3);
        list.add(set4);

        assertEquals(expected,ListDemo.addition(list,setTest));

    }

    @Test
    void oldMen1() {
        List<Human> humans = new LinkedList<>();
        humans.add(h181);
        humans.add(h182);
        humans.add(h201);
        humans.add(h202);
        Set<Human> set = new HashSet<>();
        set.add(h201);
        set.add(h202);
        assertEquals(set,ListDemo.oldMen(humans));
    }

    @Test
    void oldMen2() {
        List<Human> humans = new LinkedList<>();
        humans.add(h181);
        humans.add(h182);
        humans.add(h201);
        humans.add(h202);
        humans.add(h211);
        Set<Human> set = new HashSet<>();
        set.add(h211);
        assertEquals(set,ListDemo.oldMen(humans));
    }

    @Test
    void sortedList() {
        SortedSet<Human> expected = new TreeSet<>();
        expected.add(h181);
        expected.add(h182);
        expected.add(h201);
        expected.add(h202);
        expected.add(h211);
        List<Human> expectedList = new LinkedList<>(expected);

        assertEquals(expectedList,ListDemo.sortedList(expected));

    }

    @Test
    void sortedList1() throws HumanException {
        Set<Human> actual = new HashSet<>();
        actual.add(h211);
        actual.add(h201);
        actual.add(h202);
        actual.add(h182);
        actual.add(h181);
        Student s1 = new Student("a","b","r",20,"e");
        actual.add(s1);

        SortedSet<Human> expected = new TreeSet<>();
        expected.add(h181);
        expected.add(h182);
        expected.add(h201);
        expected.add(h202);
        expected.add(h211);
        expected.add(s1);
        List<Human> expectedList = new LinkedList<>(expected);

        assertEquals(expectedList, ListDemo.sortedList(actual));

    }

    @Test
    void idSetHuman() {
        Map<Integer,Human> integerHumanMap = new HashMap<>();
        integerHumanMap.put(1,h181);
        integerHumanMap.put(2,h182);
        integerHumanMap.put(3,h201);
        integerHumanMap.put(4,h211);
        Set<Integer> id = new HashSet<>();
        id.add(1);
        id.add(2);
        Set<Human> expected = new HashSet<>();
        expected.add(h181);
        expected.add(h182);
        assertEquals(expected,ListDemo.idSetHuman(integerHumanMap,id));
    }

    @Test
    void idSetHumanAdult() throws HumanException {
        Map<Integer,Human> integerHumanMap = new HashMap<>();
        integerHumanMap.put(1,h181);
        integerHumanMap.put(2,h182);
        integerHumanMap.put(3,h201);
        integerHumanMap.put(4,h211);
        integerHumanMap.put(5,new Human("1","3","4",4));
        integerHumanMap.put(6,new Human("2","3","4",7));
        Set<Integer> id = new HashSet<>();
        id.add(1);
        id.add(2);
        id.add(5);
        id.add(6);
        Set<Human> expected = new HashSet<>();
        expected.add(h181);
        expected.add(h182);
        assertEquals(expected,ListDemo.idSetHumanAdult(integerHumanMap,id));
    }

    @Test
    void newMapIdToAge() throws HumanException {
        Map<Integer,Human> humanMap = new HashMap<>();
        humanMap.put(1,h181);
        humanMap.put(2,h182);
        humanMap.put(3,h201);
        humanMap.put(4,h211);
        humanMap.put(5,new Human("1","3","4",4));
        humanMap.put(6,new Human("2","3","4",7));
        Map<Integer,Integer> ids = new HashMap<>();
        ids.put(1,18);
        ids.put(2,18);
        ids.put(3,20);
        ids.put(4,21);
        ids.put(5,4);
        ids.put(6,7);
        assertEquals(ids,ListDemo.newMapIdToAge(humanMap));
    }

    @Test
    void peersTest() throws HumanException {
        Set<Human> humanSet = new HashSet<>();
        humanSet.add(h181); //b
        humanSet.add(h201); //b
        humanSet.add(h202); //t
        humanSet.add(h211); //a
        humanSet.add(h182); //b
        List<Human> list1 = new LinkedList<>();
        List<Human> list2 = new LinkedList<>();
        List<Human> list3 = new LinkedList<>();

        //18
        list1.add(h182);
        list1.add(h181);

        //20
        list2.add(h202);
        list2.add(h201);

        //21
        list3.add(h211);
        Map<Integer,List<Human>> map = new HashMap<>();
        map.put(18,list1);
        map.put(20,list2);
        map.put(21,list3);


        assertEquals(map,ListDemo.peers(humanSet));


    }
   /* @Test
    public void testDirectory(){
        Set<Human> humanSet = new HashSet<>();
        humanSet.add(h181); //b
        humanSet.add(h201); //b
        humanSet.add(h202); //t
        humanSet.add(h211); //a
        humanSet.add(h182); //b
        List<Human> list1 = new LinkedList<>();
        List<Human> list2 = new LinkedList<>();
        List<Human> list3 = new LinkedList<>();

        list1.add(h181); //b
        list1.add(h182);

        list2.add(h211); //a

        list3.add(h202); //t
        list3.add(h201);

        Map<Integer,Map<Character,List<Human>>> expected = new HashMap<>();
        Map<Character,List<Human>> map1 = new HashMap<>();
        Map<Character,List<Human>> map2 = new HashMap<>();
        Map<Character,List<Human>> map3 = new HashMap<>();

        map1.put('b',list1);
        map2.put('a',list2);
        map3.put('t',list3);

        expected.put(18,map1);
        expected.put(20,map3);
        expected.put(21,map3);

        assertEquals(expected,ListDemo.directory(humanSet));

    }*/
}