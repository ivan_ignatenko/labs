package Task6;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class CollectionsDemoTest {


    @Test
    void numberOfLinesWithMatchingStartTest() {
        final List<String> strings= new ArrayList<>();
        strings.add("aaaa");
        strings.add("accc");
        strings.add("aarr");
        strings.add("ba");
        strings.add("Caa");
        assertAll(
                ()->assertEquals(3,CollectionsDemo.numberOfLinesWithMatchingStart(strings,'a')),
                ()->assertEquals(0,CollectionsDemo.numberOfLinesWithMatchingStart(strings,'r')),
                ()->assertEquals(1,CollectionsDemo.numberOfLinesWithMatchingStart(strings,'b'))
        );
    }
}