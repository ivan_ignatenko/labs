
package Task4;

import Task3.Products.Product;

import java.util.Comparator;

/**
 * Класс-компаратор Для класса «Товар» из предыдущего задания (про наследование).
 * Два товара сравниваются по названию, если названия равны, то далее — по
 * описанию.
 * @param <T>
 */


public class ComparatorProduct<T extends Product> implements Comparator<Product> {
    @Override
    public int compare(Product goods1, Product goods2) {
       if(goods1.getTitle().compareTo(goods2.getTitle())==0){
           return goods1.getDescription().compareTo(goods2.getDescription());
       }
       return goods1.getTitle().compareTo(goods2.getTitle());
    }


}
