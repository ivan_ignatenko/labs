package Task4;

public enum FunctionExceptionTypes {
    INCORRECT_ARGUMENT("Incorrect argument"),
    OUT_OF_DOMAIN("Out of domain"),
    NO_SOLUTIONS("No solutions"),
    NULLPTR("Null pointer");

    private String exceptionType;
    FunctionExceptionTypes(String exceptionType) {
        this.exceptionType = exceptionType;
    }
    public String getType(){
        return exceptionType;
    }
}
