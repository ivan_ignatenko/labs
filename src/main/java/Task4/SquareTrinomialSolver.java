package Task4;

import java.util.Objects;

public class SquareTrinomialSolver {
    private SquareTrinomial poly;

    public SquareTrinomialSolver(SquareTrinomial poly) throws FunctionException, SquareException {
        this.poly = new SquareTrinomial(poly);
    }

    public double[] solve() throws FunctionException {
        double D = poly.getB()*poly.getB() - 4*poly.getA()*poly.getC();
        if(D < 0){
            throw new FunctionException(FunctionExceptionTypes.NO_SOLUTIONS.getType());
        }
        double root1 = (-poly.getB() + Math.sqrt(D))/(2 * poly.getA());
        double root2 = (-poly.getB() - Math.sqrt(D))/(2 * poly.getA());
        return new double[] {root1, root2};
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SquareTrinomialSolver solver = (SquareTrinomialSolver) o;
        return poly.equals(solver.poly);
    }

    @Override
    public int hashCode() {
        return Objects.hash(poly);
    }
}
