package Task4;
import static java.lang.Math.sin;

/**
 * Класс, реализующий интерфейс «функция одного вещественного аргумента, определенная на отрезке [a; b]»
 * Функция: A*sin(Bx)
 */

public class SinusFunction implements IFunctionOneArgument {
    private double a;
    private double b;

    private double start;
    private double end;

    public SinusFunction(double a, double b, double start, double end) throws FunctionException {
        if(a == 0 || b == 0 || end <= start){
            throw new FunctionException("Функция не может быть определена на этом отрезке. A больше/равно б");
        }
        this.a = a;
        this.b = b;
        this.start = start;
        this.end = end;
    }

    @Override
    public double getValue(double x) throws FunctionException {
        if(x > end || x < start){
            throw new FunctionException("Невозможно получить значение вне области опеределения функции");
        }
        return a * sin(b*x);
    }


    @Override
    public double getStart() {
        return start;
    }

    @Override
    public double getEnd() {
        return end;
    }
}