package Task4;
import Task3.Products.Product;

import java.util.Arrays;
import java.util.Comparator;

/**
 * Класс ComparatorDemo со статическим методом sortGoods, который получает на
 * вход массив товаров и компаратор и сортирует массив по возрастанию
 */

public class ComparatorDemo {
    public static void sortGoods(Product[] arr, Comparator<? super Product> comparator){
        Arrays.sort(arr, comparator);
    }
}
