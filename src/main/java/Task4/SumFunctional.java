package Task4;
/**
 * Класс, реализующий интерфейс функционала
 * C методом. находящим сумму значений функции на концах отрезка и в его середине.
 */

public class SumFunctional implements IFunctional{
    private double a,b;

    public SumFunctional(double a, double b) {
        this.a = a;
        this.b = b;
    }

    @Override
    public double getValue(IFunctionOneArgument function) throws FunctionException {
        if(function == null){
            throw new FunctionException("Передан null");
        }
        if(function. getStart() > a || b > function.getEnd()){
            throw new FunctionException("Функция неопределена на [a,b]");
        }
        return function.getValue(a)+function.getValue(b) +
                function.getValue((b + a)/2);
    }
}
