package Task3.Products;

import java.util.Objects;

public class PieceProduct extends Product implements IWeightProduct {
    private double weight;

    public PieceProduct(String title, String description, double weight) throws ProductException {
        super(title, description);
        if(weight < 0){
            throw new ProductException(ProductExceptionTypes.INCORRECT_WEIGHT.getType());
        }
        this.weight = weight;
    }

    public PieceProduct(PieceProduct product) throws ProductException {
        super(product);
        if(product.weight < 0){
            throw new ProductException(ProductExceptionTypes.INCORRECT_WEIGHT.getType());
        }
        this.weight = product.weight;
    }

    @Override
    public String toString() {
        return String.format("%s.%s.%f", getTitle(), getDescription(), getWeight());
    }

    @Override
    public double getWeight() {
        return weight;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PieceProduct)) return false;
        if (!super.equals(o)) return false;
        PieceProduct that = (PieceProduct) o;
        return Double.compare(that.weight, weight) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), weight);
    }
}