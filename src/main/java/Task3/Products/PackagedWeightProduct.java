package Task3.Products;

import java.util.Objects;

public class PackagedWeightProduct extends PackagedProduct implements IWeightProduct {
    private double weight;
    private WeightProduct product;

    public PackagedWeightProduct(WeightProduct product, double weight, ProductPackaging packaging) throws ProductException {
        super(product, packaging);
        if(weight < 0){
            throw new ProductException(ProductExceptionTypes.INCORRECT_WEIGHT.getType());
        }
        this.weight = weight;
    }

    public PackagedWeightProduct(PackagedWeightProduct product) throws ProductException{
        super(product);
        if(product.weight < 0){
            throw new ProductException(ProductExceptionTypes.INCORRECT_WEIGHT.getType());
        }
        this.weight = product.weight;
    }

    public PackagedWeightProduct(String title, String description, double weight, ProductPackaging packaging) throws ProductException {
        super(title, description, packaging);
        this.weight=weight;
    }

    public double getNetto(){
        return weight;
    }

    @Override
    public double getWeight(){
        return getNetto() + super.getWeight();
    }

    @Override
    public String toString(){
        return String.format("Packaging title: %s Title: %s Description: %s Netto: %f Brutto %f",
                getPackaging().getTitle(), getTitle(), getDescription(), getNetto(), getWeight());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PackagedWeightProduct)) return false;
        if (!super.equals(o)) return false;
        PackagedWeightProduct that = (PackagedWeightProduct) o;
        return Double.compare(that.weight, weight) == 0 &&
                Objects.equals(product, that.product);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), weight, product);
    }
}