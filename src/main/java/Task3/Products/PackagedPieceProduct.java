package Task3.Products;

import java.util.Objects;

public class PackagedPieceProduct extends PackagedProduct implements IWeightProduct {
    private int quantity;
    private PieceProduct product;

    public PackagedPieceProduct(PieceProduct product, int quantity, ProductPackaging packaging) throws ProductException {
        super(product,packaging);
        this.product = new PieceProduct(product);

        if (quantity < 0) {
            throw new ProductException(ProductExceptionTypes.INCORRECT_QUANTITY.getType());
        }
        this.quantity = quantity;
    }

    public PackagedPieceProduct(PackagedPieceProduct product) throws ProductException {
        super(product);
        this.product = new PieceProduct(product.product);
        if (product.quantity < 0) {
            throw new ProductException(ProductExceptionTypes.INCORRECT_QUANTITY.getType());
        }
        this.quantity = product.quantity;
    }

    public PackagedPieceProduct(String title, String description, int quantity, ProductPackaging packaging) throws ProductException {
        super(title, description, packaging);
        this.quantity=quantity;
    }

    public int getQuantity() {
        return quantity;
    }

    public double getNetto() {
        return product.getWeight() * quantity;
    }

    @Override
    public double getWeight() {
        return getNetto() + super.getWeight();
    }

    @Override
    public String toString() {
        return String.format("Packaging title: %s Title: %s Description: %s Quantity %d Netto: %f Brutto %f",
                getPackaging().getTitle(), getTitle(), getDescription(), quantity, getNetto(), getWeight());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PackagedPieceProduct)) return false;
        if (!super.equals(o)) return false;
        PackagedPieceProduct that = (PackagedPieceProduct) o;
        return quantity == that.quantity &&
                Objects.equals(product, that.product);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), quantity, product);
    }
}
