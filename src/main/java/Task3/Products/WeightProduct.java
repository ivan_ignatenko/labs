package Task3.Products;

public class WeightProduct extends Product {

    public WeightProduct(String title, String description) throws ProductException {
        super(title, description);
    }

    public WeightProduct(WeightProduct product) throws ProductException {
        super(product);
    }
}