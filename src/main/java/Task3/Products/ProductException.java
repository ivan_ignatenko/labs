package Task3.Products;

public class ProductException extends Exception{

    public ProductException(){
        super();
    }

    public ProductException(String s){
        super(s);
    }

    public ProductException(Exception e){
        super(e);
    }

    public ProductException(String s, Exception e){
        super(s, e);
    }
}