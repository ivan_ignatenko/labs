package Task3.Products;

public enum ProductExceptionTypes {
    INCORRECT_WEIGHT("Incorrect weight"),
    INCORRECT_QUANTITY("Incorrect quantity"),
    NULLPTR("Null pointer"),
    INCORRECT_PRODUCT("Incorrect product"),
    KEK("kek");
    private String exceptionType;
    ProductExceptionTypes(String exceptionType){
        this.exceptionType=exceptionType;
    }
    public String getType(){
        return exceptionType;
    }
}