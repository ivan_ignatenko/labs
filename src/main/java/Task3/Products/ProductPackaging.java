package Task3.Products;

import java.util.Objects;

public class ProductPackaging {
    private String title;
    private double weight;

    public ProductPackaging(String title, double weight) throws ProductException {
        if(title == null || title.isEmpty()){
            throw new ProductException(ProductExceptionTypes.NULLPTR.getType());
        }
        if(weight < 0){
            throw new ProductException(ProductExceptionTypes.INCORRECT_WEIGHT.getType());
        }
        this.title = title;
        this.weight = weight;
    }

    public ProductPackaging(ProductPackaging pack) throws ProductException {
       if(pack == null){
            throw new ProductException(ProductExceptionTypes.NULLPTR.getType());
        }
        if(pack.weight < 0){
            throw new ProductException(ProductExceptionTypes.INCORRECT_WEIGHT.getType());
        }
        this.title = pack.title;
        this.weight = pack.weight;
    }

    public String getTitle(){
        return title;
    }

    public double getWeight(){
        return weight;
    }

    public String toString(){
        return String.format("%s=%f", title, weight);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ProductPackaging that = (ProductPackaging) o;
        return Double.compare(that.weight, weight) == 0 &&
                title.equals(that.title);
    }

    @Override
    public int hashCode() {
        return Objects.hash(title, weight);
    }
}