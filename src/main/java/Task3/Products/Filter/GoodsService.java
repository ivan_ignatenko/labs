package Task3.Products.Filter;

import Task3.Products.Consignment;
import Task3.Products.PackagedWeightProduct;
import Task3.Products.ProductException;

public class GoodsService {
    public static int countByFilter(Consignment consignment, Filter filter) throws ProductException {
        int num=0;
        for(int i=0;i<consignment.length();i++){
            if(filter.apply(consignment.getElement(i).getTitle())){
                num++;
            }
        }
        return num;
    }
    public static boolean checkAllWeighted(Consignment consignment) throws ProductException {
        for(int i=0;i<consignment.length();i++){
            if(!(consignment.getElement(i) instanceof PackagedWeightProduct)){
                return false;
            }
        }
        return true;
    }
}
