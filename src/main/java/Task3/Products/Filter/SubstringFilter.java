package Task3.Products.Filter;

public class SubstringFilter implements Filter {
    private String pattern;

    public SubstringFilter(String pattern) throws FilterException {
        if(pattern == null || pattern.isEmpty()){
            throw new FilterException("Incorrect pattern");
        }
        this.pattern = pattern;
    }

    @Override
    public boolean apply(String str) {
        return str.contains(pattern);
    }
}