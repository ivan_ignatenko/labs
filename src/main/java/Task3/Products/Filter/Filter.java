package Task3.Products.Filter;

public interface Filter {
    boolean apply(String str);
}
