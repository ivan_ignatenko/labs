package Task3.Products.Filter;

public class FilterException extends Exception {
    public FilterException() {
        super();
    }

    public FilterException(String s) {
        super(s);
    }

    public FilterException(Exception e) {
        super(e);
    }

    public FilterException(String s, Exception e) {
        super(s, e);
    }
}