package Task3.Products;

import java.util.Arrays;
import java.util.Objects;

public class Consignment {
    private String description;
    private PackagedProduct[] products;

    public Consignment(String description, PackagedProduct ... products) throws ProductException {
        if(description == null || description.isEmpty() || products == null){
            throw new ProductException(ProductExceptionTypes.KEK.getType());
        }
        this.description = description;
        this.products = new PackagedProduct[products.length];
        for(int i = 0; i < products.length; i++){
            this.products[i] = new PackagedProduct(products[i]);
        }
    }

    public double getWeight(){
        double weight = 0;
        for (PackagedProduct product : products) {
            weight += product.getWeight();
        }
        return weight;
    }

    public PackagedProduct getElement(int i) throws ProductException {
        if(i<0||i>products.length){
            throw new ProductException(ProductExceptionTypes.INCORRECT_QUANTITY.getType());
        }
        if(products[i] instanceof PackagedWeightProduct){
            return new PackagedWeightProduct(products[i].getTitle(),products[i].getDescription(),products[i].getWeight(),
                    products[i].getPackaging());
        }
        return new PackagedProduct(products[i].getTitle(),products[i].getDescription(),products[i].getPackaging());
    }

    public PackagedProduct[] getProducts(){
        return products;
    }

    public int length(){
        return products.length;
    }
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Consignment that = (Consignment) o;
        return description.equals(that.description) &&
                Arrays.equals(products, that.products);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(description);
        result = 31 * result + Arrays.hashCode(products);
        return result;
    }
}