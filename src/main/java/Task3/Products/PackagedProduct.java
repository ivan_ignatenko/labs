package Task3.Products;

import java.util.Objects;

public class PackagedProduct extends Product implements IWeightProduct {
    private ProductPackaging packaging;

    public PackagedProduct(String title, String description, ProductPackaging packaging) throws ProductException {
        super(title, description);
        this.packaging = new ProductPackaging(packaging);
    }

    public PackagedProduct(Product product, ProductPackaging packaging) throws ProductException {
        super(product);
        this.packaging = new ProductPackaging(packaging);
    }

    public PackagedProduct(PackagedProduct product) throws ProductException {
        super(product);
        this.packaging = new ProductPackaging(product.packaging);
    }

    @Override
    public double getWeight(){
        return packaging.getWeight();
    }

    public ProductPackaging getPackaging() {
        return packaging;
    }

    @Override
    public String toString(){
        return String.format("Package title: %s Package weight: %f Title: %s Description: %s",
                packaging.getTitle(), packaging.getWeight(), getTitle(), getDescription());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        PackagedProduct that = (PackagedProduct) o;
        return packaging.equals(that.packaging);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), packaging);
    }
}