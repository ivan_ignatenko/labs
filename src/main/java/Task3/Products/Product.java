package Task3.Products;

import java.util.Objects;

public class Product {
    private String title;
    private String description;

    public Product(String title, String description) throws ProductException {
        if(title == null || description == null || title.isEmpty() || description.isEmpty()){
            throw new ProductException(ProductExceptionTypes.NULLPTR.getType());
        }
        this.description = description;
        this.title = title;
    }

    public Product(Product product) throws ProductException {
        if(product == null){
            throw new ProductException(ProductExceptionTypes.NULLPTR.getType());
        }
        if(product.title == null || product.description == null || product.title.isEmpty() || product.description.isEmpty()){
            throw new ProductException(ProductExceptionTypes.NULLPTR.getType());
        }
        this.description = product.description;
        this.title = product.title;
    }

    public String getTitle(){
        return title;
    }

    public String getDescription(){
        return description;
    }

    @Override
    public String toString(){
        return String.format("%s.%s", title, description);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Product)) return false;
        Product product = (Product) o;
        return Objects.equals(title, product.title) &&
                Objects.equals(description, product.description);
    }

    @Override
    public int hashCode() {
        return Objects.hash(title, description);
    }
}