package Task7;

import java.io.*;

public class StreamService {
    public static void writeIntArrToByteStream(int[] arr, OutputStream o)
            throws IOException {
        try (DataOutputStream dos = new DataOutputStream(o)) {
            for (int i : arr) {
                o.write(i);
            }
        }
    }
    public static int[] readIntArrFromByteStream(InputStream in) throws IOException {
        try(DataInputStream din = new DataInputStream(in)){
            int[] arr = new int[din.available()/4];
            int i = 0;
            while(din.read() !=-1){
                arr[i] = din.readInt();
                i++;
            }
            return arr;
        }
    }
}

