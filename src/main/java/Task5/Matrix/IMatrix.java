package Task5.Matrix;

public interface IMatrix {
    double get(int i, int j) throws MatrixException;
    void set(int i, int j, double elm) throws MatrixException;
    double getDet() throws MatrixException;
}
