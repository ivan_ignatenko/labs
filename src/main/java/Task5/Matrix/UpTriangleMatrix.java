package Task5.Matrix;

public class UpTriangleMatrix extends Matrix implements IMatrix {

    public UpTriangleMatrix(int N) throws MatrixException {
        super(N);
    }

    @Override
    public void set(int i, int j, double elm) throws MatrixException {
        if(i > j && elm != 0){
            throw new MatrixException(MatrixExceptionTypes.INCORRECT_INDEX.getType());
        }
        super.set(i, j, elm);
    }

}
