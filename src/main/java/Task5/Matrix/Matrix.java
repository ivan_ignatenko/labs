package Task5.Matrix;

import java.util.Arrays;
import java.util.Objects;

public class Matrix implements IMatrix {
    private int N;
    private double[] body;
    private double det;
    private boolean isDetGetted;

    public Matrix(int N) throws MatrixException {
        if (N <= 0) {
            throw new MatrixException(MatrixExceptionTypes.INCORRECT_SIZE.getType());
        }
        this.N = N;
        body = new double[N * N];
    }

    protected Matrix(double... elms) throws MatrixException {
        if (elms == null || elms.length == 0) {
            throw new MatrixException(MatrixExceptionTypes.NULL_POINTER.getType());
        }
        N = elms.length;
        body = new double[elms.length*elms.length];
        for (int i = 0; i < elms.length; i++) {
            body[i * N + i] = elms[i];
        }
    }

    public Matrix(Matrix matrix) throws MatrixException {
        if (matrix == null || matrix.body == null || matrix.body.length == 0 || matrix.N == 0) {
            throw new MatrixException(MatrixExceptionTypes.NULL_POINTER.getType());
        }
        this.body = new double[matrix.N*matrix.N];
        this.body = Arrays.copyOf(matrix.body, matrix.body.length);
        this.N = matrix.N;
    }


    @Override
    public double get(int i, int j) throws MatrixException {
        if (i >= N || j >= N) {
            throw new MatrixException(MatrixExceptionTypes.INCORRECT_INDEX.getType());
        }
        return body[i * N + j];
    }

    @Override
    public void set(int i, int j, double elm) throws MatrixException {
        if (i >= N || j >= N) {
            throw new MatrixException(MatrixExceptionTypes.INCORRECT_INDEX.getType());
        }
        body[i * N + j] = elm;
        isDetGetted = false;
    }

    @Override
    public double getDet() throws MatrixException {
        if(isDetGetted) {
            return det;
        } else {
            det = 1;
            Matrix tmp = new Matrix(this);
            for(int j = 0; j < N; j++){
                if(tmp.get(j,j) == 0){
                    for(int i = j; i < N; i++){
                        if(tmp.get(i,j) != 0){
                            tmp.swap(i, j);
                            det *= -1;
                            break;
                        }
                        if(i == N-1 && tmp.get(N - 1, j) == 0){
                            det = 0;
                            isDetGetted = true;
                            return det;
                        }
                    }
                }
                for(int i = j + 1; i < N; i++){
                    tmp.substract(i,j, tmp.get(i,j)/tmp.get(j,j));
                }
            }
            for(int i = 0; i < N; i++){
                det *= tmp.get(i,i);
            }
            isDetGetted = true;
            return det;
        }
    }

    private void swap(int i, int j){
        double tmp;
        for(int k = 0; k < N; k++){
            tmp = body[i*N + k];
            body[i*N + k] = body[j*N + k];
            body[j*N + k] = tmp;
        }
    }

    private void substract(int i, int j, double a){
        for(int k = 0; k < N; k++){
            body[i*N + k] -= a*body[j*N + k];
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Matrix)) return false;
        Matrix matrix = (Matrix) o;
        return N == matrix.N &&
                Double.compare(matrix.det, det) == 0 &&
                isDetGetted == matrix.isDetGetted &&
                Arrays.equals(body, matrix.body);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(N, det, isDetGetted);
        result = 31 * result + Arrays.hashCode(body);
        return result;
    }
}
