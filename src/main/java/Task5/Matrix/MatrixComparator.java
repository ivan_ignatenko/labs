package Task5.Matrix;

import java.util.Comparator;

public class MatrixComparator<T extends Matrix> implements Comparator<Matrix> {
    @Override
    public int compare(Matrix o1, Matrix o2) {
        try {
            return (int)(o1.getDet() - o2.getDet());
        } catch (MatrixException e) {
            return 0;
        }
    }
}
