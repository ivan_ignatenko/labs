package Task5.Matrix;

public class DiagMatrix extends Matrix implements IMatrix {

    public DiagMatrix(int N) throws MatrixException {
        super(N);
    }

    public DiagMatrix(double ... elms) throws MatrixException {
        super(elms);
    }

    @Override
    public void set(int i, int j, double elm) throws MatrixException {
        if(i != j && elm != 0){
            throw new MatrixException(MatrixExceptionTypes.INCORRECT_INDEX.getType());
        }
        super.set(i, j, elm);
    }
}
