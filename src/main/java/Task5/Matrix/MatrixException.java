package Task5.Matrix;

public class MatrixException extends Exception {
    public MatrixException(){
        super();
    }
    public MatrixException(String s){
        super(s);
    }
    public MatrixException(Exception e){
        super(e);
    }
    public MatrixException(String s, Exception e){
        super(s,e);
    }
}
