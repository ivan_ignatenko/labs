package Task5.Matrix;

public enum MatrixExceptionTypes {
    INCORRECT_INDEX("Incorrect index"),
    NULL_POINTER("Null pointer"),
    INCORRECT_SIZE("Incorrect size");

    private String exceptionType;
    MatrixExceptionTypes(String exceptionType){
        this.exceptionType = exceptionType;
    }
    public String getType(){
        return exceptionType;
    }
}
