package Task5.Matrix;

import java.util.Arrays;

public class MatrixService {

    public static void arrangeMatrices(Matrix... mats) throws MatrixException {
        if(mats == null || mats.length == 0){
            throw new MatrixException(MatrixExceptionTypes.NULL_POINTER.getType());
        }
        for (Matrix mat : mats) {
            if (mat == null) {
                throw new MatrixException(MatrixExceptionTypes.NULL_POINTER.getType());
            }
        }
        Arrays.sort(mats, new MatrixComparator<>());
    }
}
