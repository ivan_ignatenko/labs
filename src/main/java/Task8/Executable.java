package Task8;

public interface Executable {
    void execute();
}
