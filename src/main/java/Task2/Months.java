package Task2;

import Task2.Exception.FinanceExeption;

public enum Months {
    JAN("January",1),
    FEB("February ",2),
    MAR("Marth",3),
    APR("April",4),
    MAY("May",5),
    JUN("June",6),
    JUL("July",7),
    AUG("August",8),
    SEB("September",9),
    OCT("October",10),
    NOV("November",11),
    DEC("December",12);
    private String string;
    private int index;
    Months(String string,int index){
        this.string=string;
        this.index=index;
    }
    public int getIndex() {
        return index;
    }
    public static String getMonthByIndex(int i) throws FinanceExeption {
        if(i==1){
            return JAN.getMonth();
        }
        if(i==2){
            return FEB.getMonth();
        }
        if(i==3){
            return MAR.getMonth();
        }
        if(i==4){
            return APR.getMonth();
        }
        if(i==5){
            return MAY.getMonth();
        }
        if(i==6){
            return JUN.getMonth();
        }
        if(i==7){
            return JUL.getMonth();
        }
        if(i==8){
            return AUG.getMonth();
        }
        if(i==9){
            return SEB.getMonth();
        }
        if(i==10){
            return OCT.getMonth();
        }
        if(i==11){
            return DEC.getMonth();
        }
        throw new FinanceExeption("Incorrect Month");
    }
    public String getMonth() {
        return string;
    }
}
