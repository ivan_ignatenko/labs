package Task2;

import Task2.Exception.FinanceErrorCode;
import Task2.Exception.FinanceExeption;

import java.util.Objects;

public class Date {
    private int year;
    private int month;
    private int day;
    public Date(int year, int month,int day) throws FinanceExeption {
        setYear(year);
        setMouth(month);
        setDay(day);
    }
    public Date(Date date) throws FinanceExeption {
        this(date.year,date.month,date.day);
    }

    public void setYear(int year) {
        this.year = year;
    }

    public void setMouth(int mouth) throws FinanceExeption{
        if (mouth < 1 || mouth > 12) {
            throw new FinanceExeption(FinanceErrorCode.UNACCEPTABLE_DATE.getErr());
        }
        this.month = mouth;
    }
    public void setDay(int day) throws FinanceExeption {
        if(day<1||day>31){
            throw new FinanceExeption(FinanceErrorCode.UNACCEPTABLE_DATE.getErr());
        }
        this.day=day;
    }
    public int getYear(){
        return year;
    }

    public int getMouth(){
        return month;
    }

    public int getDay(){return day;}

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Date date = (Date) o;
        return year == date.year &&
                month == date.month &&
                day == date.day;
    }

    @Override
    public int hashCode() {
        return Objects.hash(year, month, day);
    }
}