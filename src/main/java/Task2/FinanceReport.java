package Task2;

import Task2.Exception.FinanceErrorCode;
import Task2.Exception.FinanceExeption;

import java.util.Arrays;
import java.util.Objects;

public class FinanceReport {
    private String creator;
    private Date dateCreate;
    private Payment[] payments;

    public FinanceReport(Payment[] payments,String creator, Date dateCreate) throws FinanceExeption {
        if(payments==null||creator==null||creator.isEmpty()||dateCreate==null){
            throw new FinanceExeption(FinanceErrorCode.NULL_OBJECT.getErr());
        }
        this.creator = creator;
        this.dateCreate=new Date(dateCreate);
        this.payments = new Payment[payments.length];
        for (int i = 0; i < payments.length; i++) {
            this.payments[i] = new Payment(payments[i]);
        }
    }

    public FinanceReport(FinanceReport financeReport) throws FinanceExeption {
        this(financeReport.payments,financeReport.creator,financeReport.dateCreate);
    }

    public int getNumberOfPayment() {
        return payments.length;
    }

    public void setPayments(Payment payment, int i) throws FinanceExeption {
        if(payment==null){
            throw new FinanceExeption(FinanceErrorCode.NULL_OBJECT.getErr());
        }
        if (i < 0 || i >= payments.length) {
            throw new FinanceExeption(FinanceErrorCode.INCORRECT_INDEX.getErr());
        }
        this.payments[i] = new Payment(payment);
    }

    public Payment getPayment(int i) throws FinanceExeption{
        if (i < 0 || i >= payments.length) {
            throw new FinanceExeption(FinanceErrorCode.INCORRECT_INDEX.getErr());
        }
        return new Payment(payments[i]);
    }

    public Date getDateCreate() throws FinanceExeption {
        return new Date(dateCreate);
    }

    public String getCreator() {
        return creator;
    }

    public String toString(){
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Creator :").append(creator).append(" Date create:").append(dateCreate.getYear()).
                append(".").append(dateCreate.getMouth()).append(".").append(dateCreate.getDay()).append("\n");
        for (Payment payment : payments) {
            stringBuilder.append(payment.toString()).append("\n");
        }
        return stringBuilder.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FinanceReport that = (FinanceReport) o;
        return creator.equals(that.creator) &&
                dateCreate.equals(that.dateCreate) &&
                Arrays.equals(payments, that.payments);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(creator, dateCreate);
        result = 31 * result + Arrays.hashCode(payments);
        return result;
    }
}


