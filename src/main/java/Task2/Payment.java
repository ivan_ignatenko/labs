package Task2;

import Task2.Exception.FinanceErrorCode;
import Task2.Exception.FinanceExeption;

import java.util.Objects;

public class Payment {
    private String name;

    private Date dateOfPayment;
    private int amountOfPayment;
    public Payment(String name,int year,int month,int day,int amountOfPayment) throws FinanceExeption {
        setName(name);
        setDateOfPayment(year,month,day);
        setAmountOfPayment(amountOfPayment);
    }
    public Payment(String name, Date date, int amountOfPayment) throws FinanceExeption {
        this(name, date.getYear(), date.getMouth(),date.getDay(),amountOfPayment);
    }
    public Payment(String name,String date,int amountOfPayment) throws FinanceExeption {
        setName(name);
        String[] dataInt=date.split("\\.");
        setDateOfPayment(Integer.parseInt(dataInt[0]),(Integer.parseInt(dataInt[1])),(Integer.parseInt(dataInt[2])));
        setAmountOfPayment(amountOfPayment);
    }

    public Payment(String name,String data) throws FinanceExeption {
        this(name,data,0);
    }

    public Payment(String name,int year,int mouth,int day) throws FinanceExeption{
        this(name,new StringBuilder(year).append(".").append(mouth).append(".").append(day).toString());
    }

    public Payment(Payment payment) throws FinanceExeption {
        this(payment.name,payment.dateOfPayment.getYear(),payment.dateOfPayment.getMouth(),payment.dateOfPayment.getDay(),
                payment.amountOfPayment);
    }
    public void setName(String name)throws FinanceExeption{
        if(name==null){
            throw new FinanceExeption(FinanceErrorCode.NULL_OBJECT.getErr());
        }
        this.name= name;
    }
    public void setDateOfPayment(Date date) throws FinanceExeption {
        if(date==null){
            throw new FinanceExeption(FinanceErrorCode.NULL_OBJECT.getErr());
        }
        this.dateOfPayment = new Date(date);
    }
    public void setDateOfPayment(int year,int month,int day) throws FinanceExeption {
        this.dateOfPayment = new Date(year,month,day);
    }
    public void setAmountOfPayment(int amountOfPayment) throws FinanceExeption {
        if(amountOfPayment<0){
            throw new FinanceExeption(FinanceErrorCode.INCORRECT_AMOUNT_PAYMENT.getErr());
        }
        this.amountOfPayment= amountOfPayment;
    }
    public String getName() {
        return name;
    }

    public int getAmountOfPayment() {
        return amountOfPayment;
    }

    public Date getData(){
        return dateOfPayment;
    }

    public int[] getDataInt(){
        int[] arr = new int[3];
        arr[0]= dateOfPayment.getYear();
        arr[1]= dateOfPayment.getMouth();
        arr[2]=dateOfPayment.getDay();
        return arr;
    }
    public String toString(){
        return new StringBuilder().append(name).append(" ").append(dateOfPayment.getYear()).append(".").append(dateOfPayment.getMouth()).append(".").
                append(dateOfPayment.getDay()).append(".").append(amountOfPayment).toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Payment payment = (Payment) o;
        return amountOfPayment == payment.amountOfPayment &&
                name.equals(payment.name) &&
                dateOfPayment.equals(payment.dateOfPayment);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, dateOfPayment, amountOfPayment);
    }
}
