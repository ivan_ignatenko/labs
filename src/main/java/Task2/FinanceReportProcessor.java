package Task2;

import Task2.Exception.FinanceErrorCode;
import Task2.Exception.FinanceExeption;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class FinanceReportProcessor {
    public static FinanceReport allPaymentsForThisLetter(FinanceReport financeReportIn, char symbol) throws FinanceExeption {
        if (financeReportIn==null) {
            throw new FinanceExeption(FinanceErrorCode.NULL_OBJECT.getErr());
        }
        ArrayList<Payment> buff = new ArrayList<Payment>();
        for (int i = 0; i < financeReportIn.getNumberOfPayment(); i++) {
            if (financeReportIn.getPayment(i).getName().charAt(0) == symbol) {
                buff.add(financeReportIn.getPayment(i));
            }
        }
        return new FinanceReport(
                buff.toArray(new Payment[0]),
                financeReportIn.getCreator(),
                new Date(financeReportIn.getDateCreate())
        );
    }

    public static FinanceReport allPaymentLessThisPayment(FinanceReport financeReport, int thisPayment) throws FinanceExeption {
        if (financeReport==null) {
            throw new FinanceExeption(FinanceErrorCode.NULL_OBJECT.getErr());
        }
        if (thisPayment < 0) {
            throw new FinanceExeption(FinanceErrorCode.INCORRECT_AMOUNT_PAYMENT.getErr());
        }
        ArrayList<Payment> payments = new ArrayList<>();
        for (int i = 0; i < financeReport.getNumberOfPayment(); i++) {
            if (financeReport.getPayment(i).getAmountOfPayment() <= thisPayment) {
                payments.add(financeReport.getPayment(i));
            }
        }
        return new FinanceReport(
                payments.toArray(new Payment[0]),
                financeReport.getCreator(),
                new Date(financeReport.getDateCreate())
        );
    }

    public static int totalPaymentOnThisDate(FinanceReport financeReport,String date) throws FinanceExeption {
        int sum=0;
        Date date1 = parseDate(date);
        for(int i=0;i<financeReport.getNumberOfPayment();i++){
            if(financeReport.getPayment(i).getData().equals(date1)){
                sum=sum+financeReport.getPayment(i).getAmountOfPayment();
            }
        }
        return sum;
    }

    public static String monthInWhichThereWasNoPayment(FinanceReport financeReport,int year) throws FinanceExeption {
        Set<Integer> months = new HashSet<>();
        if(financeReport ==null){
            throw new FinanceExeption(FinanceErrorCode.NULL_OBJECT.getErr());
        }
        for(int i=0;i<financeReport.getNumberOfPayment();i++){
            if(financeReport.getPayment(i).getData().getYear()==year) {
                months.add(financeReport.getPayment(i).getData().getMouth());
            }
        }
        StringBuilder result = new StringBuilder();
        for(int i=1;i<12;i++){
            if(!months.contains(i)){
                    result.append(Months.getMonthByIndex(i)).append(" ");
            }
        }
        return result.toString();
    }

    private static Date parseDate(String date) throws FinanceExeption {
        if(date==null){
            throw new FinanceExeption(FinanceErrorCode.NULL_OBJECT.getErr());
        }
        String[] s = date.split("\\.");
        if(s.length!=3){
            throw new FinanceExeption("Incorrect date");
        }
        return new Date(Integer.parseInt(s[2]),Integer.parseInt(s[1]),Integer.parseInt(s[0]));
    }
}
