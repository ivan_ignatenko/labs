package Task2;

import org.jetbrains.annotations.NotNull;

public class StringProcessor {
    public static String stringCopy(String s, int N) throws IllegalArgumentException {
        if (N < 0) {
            throw new IllegalArgumentException();
        }
        if (N == 0) {
            return "";
        }
        StringBuilder result = new StringBuilder();
        for(int i=0;i<N;i++) {
            result.append(s);
        }
        return result.toString();
    }

    public static int numberOfEntries(String workStr, String searchStr) {
        return (workStr + "\0").split(searchStr).length - 1;
    }

    @NotNull
    public static String buildNewString(String workStr) {
        String result = workStr;
        result = result.replaceAll("1", "один");
        result = result.replaceAll("2", "два");
        result = result.replaceAll("3", "три");
        return result;
    }

    public static void deleteEvenCharInStringBuilder(StringBuilder workStr) {
        int idx = 1;
        for (int i = 1; i < workStr.length(); i++) {
            if ((i + idx) % 2 == 0) {
                workStr.deleteCharAt(i);
            }
            idx++;
        }
    }

    public static StringBuilder replaceString(StringBuilder source) {
        String[] words=source.toString().split("\\s");
        String temp = words[0];
        StringBuilder result = new StringBuilder();
        words[0]=words[words.length-1];
        words[words.length-1]=temp;
        for(int i = 0; i < words.length-1;i++){
            result.append(words[i]);
            result.append(" ");
        }
        result.append(words[words.length-1]);
        return result;
    }
    public static String convertSexteenNumberToUsualNumber(String s){
        String[] words = s.split(" ");
        StringBuilder result=new StringBuilder();
        for(int i=0;i<words.length;i++){
            if(words[i].charAt(0)=='0'&& (words[i].charAt(1)=='x'||words[i].charAt(1)=='X')){
                words[i]=convertSeexteenNumber(words[i]);
            }
            result.append(words[i]).append(" ");
        }
        return result.toString();
    }
    private static String convertSeexteenNumber(String s){
        int sum = 0;
        for(int i=0;i<s.length();i++){
            sum = (int) (sum+recognize(s.charAt(s.length()-(i+1)))*Math.pow(16,i));
        }
        return String.valueOf(sum);
    }
    private static int recognize(char s){
        if(s=='0')return 0;
        if(s=='1')return 1;
        if(s=='2')return 2;
        if(s=='3')return 3;
        if(s=='4')return 4;
        if(s=='5')return 5;
        if(s=='6')return 6;
        if(s=='7')return 7;
        if(s=='8')return 8;
        if(s=='9')return 9;
        if(s=='A' || s =='a'){
            return 10;
        }
        if(s=='B' ||s=='b' ){
            return 11;
        }
        if(s=='c'||s=='C'){
            return 12;
        }
        if(s=='D'|| s=='d'){
            return 13;
        }
        if(s=='E'||s=='e'){
            return 14;
        }
        if(s=='F'||s=='f'){
            return 15;
        }
        if(s=='x'||s=='X'){
            return 0;
        }
        return -1;
    }
}
