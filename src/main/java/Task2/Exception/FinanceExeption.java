package Task2.Exception;

public class FinanceExeption extends Exception {
    private FinanceErrorCode errorCode;
    public FinanceExeption (FinanceErrorCode errorCode){
        this.errorCode=errorCode;
    }
    public FinanceExeption() {
    }

    public FinanceExeption(String message) {
        super(message);
    }

    public FinanceExeption(String message, Throwable cause) {
        super(message, cause);
    }

    public FinanceExeption(Throwable cause) {
        super(cause);
    }

    public FinanceExeption(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
