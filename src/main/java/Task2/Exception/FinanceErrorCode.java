package Task2.Exception;

public enum FinanceErrorCode {
    UNACCEPTABLE_DATE("unacceptable date"),
    NULL_OBJECT("Null pointer :("),
    INCORRECT_AMOUNT_PAYMENT("incorrect amount payment"),
    INCORRECT_INDEX("incorrect index");
    private String err;
    FinanceErrorCode(String err){
        this.err=err;
    }

    public String getErr() {
        return err;
    }
}
