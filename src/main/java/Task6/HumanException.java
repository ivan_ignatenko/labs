package Task6;

public class HumanException extends Exception {
    public HumanException() {
    }

    public HumanException(String message) {
        super(message);
    }
}
