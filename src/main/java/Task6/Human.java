package Task6;

import org.jetbrains.annotations.NotNull;

import java.util.Objects;
import java.util.StringJoiner;

public class Human implements Comparable<Human> {
    private String firstName;
    private String lastName;
    private String patronymic;
    private int age;

    public Human(@NotNull String firstName,@NotNull String lastName,
                 @NotNull String patronymic, int age) throws HumanException {
        if(age < 1){
            throw new HumanException("недопустимый возраст");
        }
        this.firstName = firstName;
        this.lastName = lastName;
        this.patronymic = patronymic;
        this.age = age;
    }

    @NotNull
    public String getFirstName() {
        return firstName;
    }

    @NotNull
    public String getLastName() {
        return lastName;
    }

    @NotNull
    public String getPatronymic() {
        return patronymic;
    }

    public int getAge() {
        return age;
    }

    public void setFirstName(@NotNull String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(@NotNull String lastName) {
        this.lastName = lastName;
    }

    public void setPatronymic(@NotNull String patronymic) {
        this.patronymic = patronymic;
    }

    public void setAge(int age) throws HumanException {
        if(age < 1){
            throw new HumanException("недопустимый возраст");
        }
        this.age = age;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Human)) return false;
        Human human = (Human) o;
        return getAge() == human.getAge() &&
                getFirstName().equals(human.getFirstName()) &&
                getLastName().equals(human.getLastName()) &&
                getPatronymic().equals(human.getPatronymic());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getFirstName(), getLastName(), getPatronymic(), getAge());
    }


    @Override
    public String toString() {
        return new StringJoiner(lastName).add(" ").add(firstName).add(" ").add(patronymic).toString();
    }


    @Override
    public int compareTo(@NotNull Human o) {
        if(this.toString().compareTo(o.toString())==0){
            return Integer.compare(this.age,o.age);
        }
        return this.toString().compareTo(o.toString());
    }
}
