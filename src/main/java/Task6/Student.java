package Task6;

import org.jetbrains.annotations.NotNull;

import java.util.Objects;

public class Student extends Human {
    private String faculty;

    public Student(@NotNull String firstName,@NotNull String lastName,
                   @NotNull String patronymic, int age,@NotNull String faculty) throws HumanException {
        super(firstName, lastName, patronymic, age);
        this.faculty = faculty;
    }

    @NotNull
    public String getFaculty() {
        return faculty;
    }

    public void setFaculty(@NotNull  String faculty) {
        this.faculty = faculty;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Student)) return false;
        if (!super.equals(o)) return false;
        Student student = (Student) o;
        return getFaculty().equals(student.getFaculty());
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), getFaculty());
    }

}
