package Task6;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class CollectionsDemo {

    /**
     * ========   1   ========
     * @param in -список строк
     * @param symbol - символ
     * @return количество строк входного списка, у которых первый
     * символ совпадает с заданным
     */

   public static int numberOfLinesWithMatchingStart(@NotNull List<String> in, char symbol){
       int count=0;
       for(String s:in){
           if(s.charAt(0)==symbol){
               count++;
           }
       }
       return count;
   }

}
