package Task6;

import javafx.collections.transformation.SortedList;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.*;

public class ListDemo {

    /**
     * ========   2   ========
     * @param humanList - список объектов типа Human
     * и еще один объект типа Human
     * @return список однофамильцев заданного человека
     * среди людей из входного списка.
     */

    @NotNull
    public static List<Human> namesake(@NotNull List<Human> humanList, @NotNull Human human){
        List<Human> result = new ArrayList<>();
        for(Human idx: humanList){
            if(idx.getLastName().equals(human.getLastName())){
                result.add(idx);
            }
        }
        return result;
    }

    /**
     * ========   3   ========
     * @param humanList - список объектов типа Human и еще один объект типа Human
     * @param thisPerson - объект типа Human
     * @return копия
     * входного списка, не содержащая выделенного человека
     */

    @NotNull
    public static List<Human> withoutThisPerson(@NotNull List<Human> humanList, @NotNull Human thisPerson){
        List<Human> result = new ArrayList<>();
        for(Human idx: humanList){
            if(!idx.equals(thisPerson)){
                result.add(idx);
            }
        }
        return result;
    }

    /**
     * ========   4   ========
     * @param setList - список множеств целых чисел и еще одно множество
     * @param set - множество
     * @return список всех множеств
     * входного списка, которые не пересекаются с заданным множеством
     */

    @NotNull
    public static List<Set<Integer>> addition(@NotNull List<Set<Integer>> setList, @NotNull Set<Integer> set){
        List<Set<Integer>> result = new ArrayList<>();
        for(Set<Integer> idx: setList){
            if(!set.retainAll(idx)){
                result.add(idx);
            }
        }
        return result;
    }

    /**
     * ========   5   ========
     * @param humanList -  список, состоящий из
     * объектов типа Human и его производных классов
     * @return множество людей из
     * входного списка с максимальным возрастом
     */
    @NotNull
    public static Set<Human> oldMen(@NotNull List<Human> humanList){
        Set<Human> result = new HashSet<>();
        int maxage = 0;
        for(Human idx: humanList){
            if(idx.getAge()>maxage){
                maxage = idx.getAge();
            }
        }
        for(Human idx: humanList){
            if(idx.getAge()==maxage){
                result.add(idx);
            }
        }
        return result;
    }

    /**
     * ========   6   ========
     * @param humanSet - множество людей
     * @return список по возрастанию ФИО
     */

    @NotNull
    public static List<Human> sortedList(@NotNull Set<Human> humanSet){
        SortedSet<Human> treeSet = new TreeSet<>(humanSet);
        return new LinkedList<>(treeSet);
    }

    /**
     * ========   7   ========
     * @param integerHumanMap -отображение (Map)
     * целочисленных идентификаторов в объекты типа Human
     * @param ids - уникальный целочисленный
     * идентификатор
     * @return множество людей, идентификаторы которых содержатся во входном
     * множестве
     */

    @NotNull
    public static Set<Human> idSetHuman (@NotNull Map<Integer,Human> integerHumanMap,
                                         @NotNull Set<Integer> ids){
        Set<Human> result = new HashSet<>();
        Set<Integer> buff = integerHumanMap.keySet();
        for(Integer idx : buff){
            if(ids.contains(idx)){
                result.add(integerHumanMap.get(idx));
            }
        }
        return result;
    }

    /**
     * ========   8   ========
     * @param integerHumanMap -отображение (Map)
     * целочисленных идентификаторов в объекты типа Human
     * @param ids - уникальный целочисленный
     * идентификатор
     * @return множество людей, идентификаторы которых содержатся во входном
     * множестве и возраст не менее 18
     */

    @NotNull
    public static Set<Human> idSetHumanAdult (@NotNull Map<Integer,Human> integerHumanMap,
                                         @NotNull Set<Integer> ids){
        Set<Human> result = new HashSet<>();
        Set<Integer> buff = integerHumanMap.keySet();
        for(Integer idx : buff){
            if(ids.contains(idx) && integerHumanMap.get(idx).getAge()>=18){
                result.add(integerHumanMap.get(idx));
            }
        }
        return result;
    }

    /**
     * ========   9   ========
     * @param integerHumanMap -отображение (Map)
     * целочисленных идентификаторов в объекты типа Human
     * @return новое отображение, которое идентификатору
     * сопоставляет возраст человека
     */
    @NotNull
    public static Map<Integer,Integer> newMapIdToAge (@NotNull Map<Integer,Human> integerHumanMap){
        Map<Integer,Integer> result = new HashMap<>();
        Set<Integer> id = integerHumanMap.keySet();
        for(Integer idx : id){
           result.put(idx,integerHumanMap.get(idx).getAge());
        }
        return result;
    }

    /**
     *========   10   ========
     * @param humans -множество объектов типа Human
     * @return - отображение, которое целому числу
     * (возраст человека) сопоставляет список всех людей данного возраста из входного
     * множества.
     */

    @NotNull
    public static Map<Integer,List<Human>> peers(@NotNull Set<Human> humans){
        Map<Integer,List<Human>> result = new HashMap<>();
        for(Human idx: humans){
            List<Human> buff;
            if(result.get(idx.getAge()) == null){
                buff = new LinkedList<>();
            }
            else{
               buff = result.get(idx.getAge());
            }
            buff.add(idx);
            result.put(idx.getAge(),buff);
        }

        return result;

    }

   @NotNull
    public static Map<Integer,Map<Character,List<Human>>>
            directory(@NotNull Set<Human> humans){
        Map<Integer,Map<Character,List<Human>>> result = new HashMap<>();
            for(Human idx1:humans){
                if(result.get(idx1.getAge()) == null){
                    SortedMap<Character,List<Human>> buffMap = new TreeMap<>();
                    for(Human idx2:humans){
                        if(idx1.getAge() == idx2.getAge() && !idx1.equals(idx2)) {
                            if (buffMap.get(idx2.getLastName().charAt(0)) == null) {
                                List<Human> buffList = new LinkedList<>();
                                buffList.add(idx2);
                                buffMap.put(idx2.getLastName().charAt(0), buffList);
                            } else {
                                List<Human> buffList = buffMap.get(idx2.getLastName().charAt(0));
                                buffList.add(idx2);
                                buffMap.put(idx2.getLastName().charAt(0), buffList);
                            }
                        }
                    }
                    result.put(idx1.getAge(),buffMap);
                }
                else {
                    Map<Character,List<Human>> buffMap = result.get(idx1.getAge());
                    for(Human idx2:humans){
                        if(idx1.getAge() == idx2.getAge() && !idx1.equals(idx2)) {
                            if (buffMap.get(idx2.getLastName().charAt(0)) == null) {
                                List<Human> buffList = new LinkedList<>();
                                buffList.add(idx2);
                                buffMap.put(idx2.getLastName().charAt(0), buffList);
                            } else {
                                List<Human> buffList = buffMap.get(idx2.getLastName().charAt(0));
                                buffList.add(idx2);
                                buffMap.put(idx2.getLastName().charAt(0), buffList);
                            }
                        }
                    }
                    result.put(idx1.getAge(),buffMap);
                }
            }
            return result;
    }


}
