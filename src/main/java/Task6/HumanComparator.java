package Task6;

import java.util.Comparator;

public class HumanComparator<T extends Human> implements Comparator<Human> {
    @Override
    public int compare(Human o1, Human o2) {
        return o1.toString().compareTo(o2.toString());
    }
}
