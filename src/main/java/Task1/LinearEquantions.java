package Task1;

public class LinearEquantions {
    public static void TwoLinearEquations(double x1, double x2, double x3, double y1, double y2, double y3) throws Exception {
        if (Math.abs(x1 * y2 - x2 * y1) < 1e-4) {
            // System.out.println("zero");
            throw new Exception("Zero Equation");
        }
        System.out.println((double) (x3 * y2 - y3 * x2) / (x1 * y2 - x2 * y1));
        System.out.println((double) (x1 * y3 - x3 * y1) / (x1 * y2 - x2 * y1));
    }
}
 /*   public static void main(String[] args) throws Exception {
        TwoLinearEquations(0,0,0, 0,0,0);
        System.out.println( );
        TwoLinearEquations(0,0,0, 0,0,1);
        TwoLinearEquations(0,0,1, 3,2,1);
        TwoLinearEquations(1,1,2, 2,2,1);
        TwoLinearEquations(1,0,2, 2,0,1);
        TwoLinearEquations(0,1,2, 0,2,1);
        System.out.println();
        TwoLinearEquations(1,2,3,0,0,0);
        TwoLinearEquations(1,2,3,2,4,6);
        TwoLinearEquations(1,0,3,2,0,6);
        TwoLinearEquations(0,2,3,0,4,6);
        System.out.println();
        TwoLinearEquations(1,2,3,4,5,6);
    }
}*/
