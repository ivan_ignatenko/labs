package Task1;

public class Point3D {
    private double x,y,z;
    public Point3D(double x,double y,double z){
        this.x=x;
        this.y=y;
        this.z=z;
    }
    public Point3D(){
        x=0;
        y=0;
        z=0;
    }
    public double getX(){
        return x;
    }
    public double getY() {
        return y;
    }
    public double getZ(){
        return z;
    }

    public void setX(double x) {
        this.x = x;
    }

    public void setY(double y) {
        this.y = y;
    }

    public void setZ(double z) {
        this.z = z;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;
        Point3D point3D = (Point3D) obj;
        return Double.compare(point3D.x, x) == 0 &&
                Double.compare(point3D.y, y) == 0 &&
                Double.compare(point3D.z, z) == 0;
    }

    public void printPoint(){
        System.out.println("( X: "+x+" Y: "+y+" Z: "+z+")");
    }

    @Override
    public String toString() {
        return "Point3D{" +
                "x=" + x +
                ", y=" + y +
                ", z=" + z +
                '}';
    }
}


