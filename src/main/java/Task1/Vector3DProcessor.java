package Task1;

public class Vector3DProcessor {
    public static Vector3D sumVector(Vector3D v1, Vector3D v2) {
        return new Vector3D(
                v1.getX() + v2.getX(),
                v1.getY() + v2.getY(),
                v1.getZ() + v2.getZ()
        );
    }

    public static Vector3D subVector(Vector3D v1, Vector3D v2) {
        return new Vector3D(v1.getX() - v2.getX(), v1.getY() - v2.getY(), v1.getZ() - v2.getZ());
    }

    public static double scalarMul(Vector3D v1, Vector3D v2) {
        return v1.getX() * v2.getX() + v1.getY() * v2.getY() + v1.getZ() * v2.getZ();
    }

    public static Vector3D vectorMul(Vector3D v1, Vector3D v2) {
        return new Vector3D(
                v1.getY() * v2.getZ() - v1.getZ() * v2.getY(),
                v1.getZ() * v2.getX() - v1.getX() * v2.getZ(),
                v1.getX() * v2.getY() - v1.getY() * v2.getX()
        );
    }
    public static Vector3D multNum(Vector3D v, double num){
        return new Vector3D(v.getX()*num
                ,v.getY()*num
                ,v.getZ()*num);
    }
    public static boolean collinearity(Vector3D v1, Vector3D v2) {
        return (Math.abs(scalarMul(v1,v2))==Math.abs(v1.length()*v2.length()));
    }
}

