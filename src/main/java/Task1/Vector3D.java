package Task1;

import java.util.Objects;

public class Vector3D {
    private double x,y,z;
    public Vector3D(){

    }
    public Vector3D(Point3D point){
        x=point.getX();
        y=point.getY();
        z=point.getZ();
    }
    public Vector3D(double x,double y,double z){
        this.x=x;
        this.y=y;
        this.z=z;
    }

    public double length(){
        return Math.sqrt(x*x+y*y+z*z);
    }

    public double length(Vector3D o){
        return Math.sqrt(Math.pow(x-o.x,2)+Math.pow(y-o.y,2)+Math.pow(z-o.z,2));
    }
    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public double getZ() {
        return z;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Vector3D vector3D = (Vector3D) o;
        return Double.compare(vector3D.x, x) == 0 &&
                Double.compare(vector3D.y, y) == 0 &&
                Double.compare(vector3D.z, z) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(x, y, z);
    }

    @Override
    public String toString() {
        return "Vector3D{" +
                "x=" + x +
                ", y=" + y +
                ", z=" + z +
                '}';
    }
}
