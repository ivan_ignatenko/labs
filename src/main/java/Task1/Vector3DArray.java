package Task1;

public class Vector3DArray {
    private Vector3D[] vectorArr;
    public Vector3DArray(){

    }
    public Vector3DArray(int numbers){
        vectorArr = new Vector3D[numbers];
        for(int i=0;i<vectorArr.length;i++){
            vectorArr[i]=new Vector3D();
        }
    }
    public int length(){
        return vectorArr.length;
    }
    public void merge(int i, Vector3D v){
        vectorArr[i]=v;
    }
    public double maxLength(){
        double max=0;
        for(int i=0;i<vectorArr.length;i++){
            if(max>vectorArr[i].length()){
                max=vectorArr[i].length();
            }
        }
        return max;
    }
    public int searchVector(Vector3D v){
        for(int i = 0;i<vectorArr.length;i++){
            if(vectorArr[i]==v){
                return i;
            }
        }
        return -1;
    }
    public Vector3D sum(){
        Vector3D v = new Vector3D(0,0,0);
        for(int i=0;i<vectorArr.length;i++){
            v = Vector3DProcessor.sumVector(v,vectorArr[i]);
        }
        return v;
    }
    public Vector3D linComb(float[] koefs) throws IllegalArgumentException {
        if(vectorArr.length != koefs.length) throw new IllegalArgumentException();
        else{
            Vector3D resultVect = new Vector3D();
            for(int i = 0; i < vectorArr.length; i++){
               resultVect= Vector3DProcessor.sumVector(resultVect, Vector3DProcessor.multNum(vectorArr[i],koefs[i]));
            }
            return resultVect;
        }
    }

    public Point3D[] pointShift(Point3D point){
        Point3D[] resPoints = new Point3D[vectorArr.length];
        for(int i = 0; i < vectorArr.length; i++){
            resPoints[i].setX(vectorArr[i].getX() + point.getX());
            resPoints[i].setY(vectorArr[i].getY() + point.getY());
            resPoints[i].setZ(vectorArr[i].getZ() + point.getZ());
        }
        return resPoints;
    }
}
